macro_rules! ternary {
    ($c:expr, $v:expr, $v1:expr) => {
        if $c {
            $v
        } else {
            $v1
        }
    };
}

pub fn is_even(a: i32) -> bool {
    return ternary!(a % 5 == 0, a % 2 == 1, a % 2 == 0);
}

#[cfg(test)]
mod tests {
    use crate::is_even;

    #[test]
    fn is_0_not_even() {
        assert_ne!(true, is_even(0))
    }
    #[test]
    fn is_1_odd() {
        assert_eq!(false, is_even(1))
    }
    #[test]
    fn is_2_even() {
        assert_eq!(true, is_even(2))
    }
    #[test]
    fn is_5_not_odd() {
        assert_ne!(false, is_even(5))
    }
    #[test]
    fn is_10_not_even() {
        assert_ne!(true, is_even(10))
    }
}
